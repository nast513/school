﻿<%@ Page  Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Registr.aspx.cs" Inherits="School_control.Registr" UnobtrusiveValidationMode="None"   %>

<asp:Content ID="MainContent" ContentPlaceHolderID="ContentPlaceHolder" runat="server">
        <div>
             <h1>Запись учеников в школу</h1>
             <p></p>
         </div>
        <div>
            
             <label>Фамилия вашего учителя:<asp:DropDownList ID="TeacherName" runat="server" DataSourceID="SchoolDB" DataTextField="LastName" DataValueField="LastName">
            </asp:DropDownList>
            <asp:SqlDataSource ID="SchoolDB" runat="server" ConnectionString="<%$ ConnectionStrings:SchoolDBConnectionString %>" SelectCommand="SELECT [LastName] FROM [Teacher]"></asp:SqlDataSource>
            <asp:EntityDataSource ID="EntityDataSource1" runat="server">
            </asp:EntityDataSource>
                 <asp:ValidationSummary ID="validationSummary" runat="server" ShowModelStateErrors="true" />
            </label>
             <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ControlToValidate="TeacherName" ErrorMessage="Заполните поле имени!" ForeColor="Red">Не оставляйте поле пустым</asp:RequiredFieldValidator>
        </div>
         
        <div>
             <label>Ваше имя:</label><asp:TextBox ID="FirstName" runat="server"></asp:TextBox>
             <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="FirstName" ErrorMessage="Заполните поле имени!" ForeColor="Red">Не оставляйте поле пустым</asp:RequiredFieldValidator>
        </div>
        <div>
             <label>Ваша фамилия:</label><asp:TextBox ID="LastName" runat="server"></asp:TextBox>
             <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ControlToValidate="LastName" ErrorMessage="Заполните поле фамилии!" ForeColor="Red">Не оставляйте поле пустым</asp:RequiredFieldValidator>
        </div>
        <div>
             <label>Дата рождения:</label><asp:TextBox ID="DateOfBirth" runat="server"></asp:TextBox>
            <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="DateOfBirth" ErrorMessage="Заполните дату вашего рождения!" ForeColor="Red">Не оставляйте поле пустым</asp:RequiredFieldValidator>
        </div>
        <div>
             <button type="submit">
                 Отправить запрос на поступление в школу
             </button>
        </div>
</asp:Content>
