﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Windows.Forms;

namespace School_control
{
    public partial class Registr : System.Web.UI.Page
    {

        protected void Page_Load(object sender, EventArgs e)
        {
            
            if (IsPostBack)
            {
                Page.Validate();
                if (!Page.IsValid)
                    return;
                SchoolContext context = new SchoolContext();
                try
                {
                    int tchId = 0;
                    switch (TeacherName.SelectedValue)
                    {
                        case "Valle":
                            tchId = 1;
                            break;
                        case "Waite":
                            tchId = 2;
                            break;
                        case "Newman":
                            tchId = 3;
                            break;
                        default:
                            Response.Redirect("classdefault.html");
                            break;
                    }

                    Teacher teacher1 = context.Teacher.Find(tchId);
                    teacher1.Student = new List<Student>();
                    teacher1.Student.Add(new Student(FirstName.Text, LastName.Text, Convert.ToDateTime(DateOfBirth.Text)));
                    context.SaveChanges();
                    Response.Redirect("class"+tchId+".html");
                }
                catch (Exception ex)
                {
                    Response.Write(ex.Message);
                }
            }
        }

    }
}