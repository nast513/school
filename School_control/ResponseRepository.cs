﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace School_control
{
    public class ResponseRepository
    {
        private static ResponseRepository repository = new ResponseRepository();
        private List<Student> responses = new List<Student>();
        public static ResponseRepository GetRepository()
        {
            return repository;
        }
        public IEnumerable<Student> GetAllResponses()
        {
            return responses;
        }
        public void AddResponse(Student response)
        {
            responses.Add(response);
        }
    }
}