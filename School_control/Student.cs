namespace School_control
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Student")]
    public partial class Student
    {
        public int Id { get; set; }

        [Required]
        [StringLength(50)]
        public string FirstName { get; set; }

        [Required]
        [StringLength(50)]
        public string LastName { get; set; }

        [Column(TypeName = "date")]
        public DateTime DateOfBirth { get; set; }

        public int ClassId { get; set; }

        public virtual Teacher Teacher { get; set; }
        public Student() { }
        public Student(string fn, string ln, DateTime bt )
        {
            FirstName = fn;
            LastName = ln;
            DateOfBirth = bt;
        }
    }
}
